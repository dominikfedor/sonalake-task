const path = require('path');
const webpack = require('webpack');

const CompressionPlugin = require('compression-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const GitRevisionPlugin = require('git-revision-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const BrotliPlugin = require('brotli-webpack-plugin');
const mergeRight = require('ramda/src/mergeRight');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const ImageminWebpWebpackPlugin = require('imagemin-webp-webpack-plugin');

const baseConfig = require('./base');

// Postcss settings
const postCssSettings = {
    discardComments: {
        removeAll: true
    },
    plugins() {
        return [
            require('autoprefixer'),
            require('cssnano')()
        ];
    },
    sourceMap: true
};

const GitRevision = new GitRevisionPlugin({
    lightweightTags: true
});

const config = mergeRight(baseConfig, {
    entry: path.join(__dirname, '../src/index.tsx'),
    cache: false,
    devtool: 'source-map',
    mode: 'production',
    output: mergeRight(baseConfig.output, {
        filename: 'app.[hash].js'
    }),
    optimization: {
        minimize: true,
        minimizer: [new TerserPlugin({
            sourceMap: true,
            terserOptions: {
                output: {
                    comments: false,
                },
            },
            extractComments: false,
        })],
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false
        }),
        new webpack.DefinePlugin({
            WEBPACK_GIT_VERSION: JSON.stringify(GitRevision.version()),
            'process.env': {
                NODE_ENV: JSON.stringify('staging'),
                // TODO: TBA
                REST_URL: JSON.stringify(''),
                REST_VERSION: JSON.stringify('application/json'),
                GRAPHQL_URL: JSON.stringify(''),
            }
        }),
        new webpack.optimize.AggressiveMergingPlugin(),
        new ImageminPlugin({
            pngquant: {
                quality: '65-75'
            }
        }),
        new ImageminWebpWebpackPlugin(),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output, both options are optional
            filename: 'app.[hash].css',
            chunkFilename: 'app.[id].[hash].css'
        }),
        new HtmlWebpackPlugin({
            appHash: GitRevision.commithash(),
            appVersion: GitRevision.version(),
            favicon: path.join(__dirname, '../src/assets/favicon.ico'),
            filename: '../index.html',
            inject: false,
            minify: {
                collapseWhitespace: true,
                minifyCSS: true,
                minifyJS: true,
                removeComments: true
            },
            template: path.join(__dirname, '../src/index.production.html.ejs'),
            xhtml: true
        }),
        new CompressionPlugin({
            minRatio: 0.5,
            test: /\.js$|\.css$|\.html$/
        }),
        new BrotliPlugin({
            minRatio: 0.5,
            test: /\.(js|css|html|svg)$/,
        })
    ]
});

// Add loaders
config.module.rules.push({
    test: /\.css/,
    use: [
        MiniCssExtractPlugin.loader, {
            loader: 'css-loader',
            options: {
                importLoaders: 1,
                sourceMap: true
            }
        }, {
            loader: 'postcss-loader',
            options: postCssSettings
        }
    ]
}, {
    test: /\.scss/,
    use: [
        MiniCssExtractPlugin.loader, {
            loader: 'css-loader',
            options: {
                importLoaders: 1,
                sourceMap: true
            }
        }, {
            loader: 'postcss-loader',
            options: postCssSettings
        }, {
            loader: 'sass-loader',
            options: {
                sourceMap: true
            }
        }
    ]
}, {
    test: /\.less/,
    use: [
        MiniCssExtractPlugin.loader, {
            loader: 'css-loader',
            options: {
                importLoaders: 1,
                sourceMap: true
            }
        }, {
            loader: 'postcss-loader',
            options: postCssSettings
        }, {
            loader: 'less-loader',
            options: {
                sourceMap: true
            }
        }
    ]
});

module.exports = config;
