const webpack = require('webpack');
const mergeRight = require('ramda/src/mergeRight');

const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const GitRevisionPlugin = require('git-revision-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const baseConfig = require('./base');

// Postcss settings
const postCssSettings = {
    discardComments: {
        removeAll: true
    },
    plugins() {
        return [
            require('autoprefixer'),
            require('cssnano')()
        ];
    },
    sourceMap: true
};

const GitRevision = new GitRevisionPlugin();
const PORT = 10000;

const config = mergeRight(baseConfig, {
    mode: 'development',
    entry: [
        `webpack-dev-server/client?http://127.0.0.1:${PORT}`,
        './src/index.tsx'
    ],
    output: mergeRight(baseConfig.output, {
        filename: 'app.js'
    }),
    cache: true,
    devtool: 'eval-source-map',
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: false,
            debug: true
        }),
        new webpack.DefinePlugin({
            WEBPACK_GIT_VERSION: JSON.stringify(GitRevision.version()),
            WEBPACK_APP_PORT: PORT,
            'process.env': {
                NODE_ENV: JSON.stringify('development'),
                REST_URL: JSON.stringify('http://localhost:3000'),
                REST_VERSION: JSON.stringify('application/json'),
                GRAPHQL_URL: JSON.stringify('https://graphql-pokemon.now.sh'),
            }
        }),
        new BrowserSyncPlugin({
            open: false,
            host: 'localhost',
            port: PORT + 1,
            proxy: `http://localhost:${PORT}`,
            ui: {
                port: PORT + 2
            }
        }, {
            // prevent BrowserSync from reloading the page and let Webpack Dev Server take care of this
            reload: false
        }),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output, both options are optional
            filename: '[name].css',
            chunkFilename: '[id].css'
        })
    ],
    performance: {
        hints: false
    }
});

// Add loaders
config.module.rules.push({
    test: /\.css/,
    use: [
        'style-loader', {
            loader: 'css-loader',
            options: {
                importLoaders: 1,
                sourceMap: true
            }
        }, {
            loader: 'postcss-loader',
            options: postCssSettings
        }
    ]
}, {
    test: /\.scss/,
    use: [
        'style-loader', {
            loader: 'css-loader',
            options: {
                importLoaders: 1,
                sourceMap: true
            }
        }, {
            loader: 'postcss-loader',
            options: postCssSettings
        }, {
            loader: 'sass-loader',
            options: {
                sourceMap: true
            }
        }
    ]
});

module.exports = config;
