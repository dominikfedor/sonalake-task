const path = require('path');
const GitRevisionPlugin = require('git-revision-webpack-plugin');
const logger = require('webpack-log');

const SRC_PATH = path.join(__dirname, '/../src');
const PUBLIC_PATH = '/assets/';

const GitRevision = new GitRevisionPlugin({ lightweightTags: true });
logger().info(`Building version: ${GitRevision.version()}`);

module.exports = {
    output: {
        path: path.join(__dirname, '/../dist/assets'),
        publicPath: PUBLIC_PATH
    },
    devServer: {
        compress: true,
        contentBase: './src/',
        historyApiFallback: true,
        publicPath: PUBLIC_PATH,
        stats: 'normal'
    },
    resolve: {
        extensions: ['.ts', '.js', '.json', '.tsx'],
        alias: {
            components: `${SRC_PATH}/components/`,
            images: `${SRC_PATH}/assets/images`,
            hooks: `${SRC_PATH}/hooks/`,
            reducers: `${SRC_PATH}/foundation/reducers`,
            models: `${SRC_PATH}/foundation/models`,
        }
    },
    module: {
        rules: [
            {
            test: /\.(js|jsx|tsx|ts)$/,
            include: path.join(__dirname, '/../src'),
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['react-app']
                }
            }
        }, {
            test: /\.(jpe?g|png|gif|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            use: [{
                loader: 'file-loader',
            }, {
                loader: 'image-webpack-loader',
                options: {
                    bypassOnDebug: true
                }
            }]
        }, {
            test: /\.(otf|ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            use: [{
                loader: 'file-loader',
            }]
        }, {
            test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            use: [{
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    mimetype: 'application/font-woff'
                }
            }]
        }]
    }
};