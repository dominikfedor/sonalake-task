export const INITIAL_NETWORK_REDUCER_STATE = {
    result: null,
    error: null,
    loading: false,
    info: { changed: false },
};