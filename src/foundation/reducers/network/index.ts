import { NetworkAction } from "../../models/network";

import { ReducerStateMap, ReducerState } from "./ReducerState";
import { Action } from "./Action";

import { INITIAL_NETWORK_REDUCER_STATE } from "./InitialState";

export const fetchReducer = <S>(state: ReducerState<S>, { type, payload = INITIAL_NETWORK_REDUCER_STATE }: Action) => {
    const map: ReducerStateMap<S> = {
        [NetworkAction.Loading]: ({ ...state, loading: true }),
        [NetworkAction.Success]: ({ ...payload, loading: false, error: null }),
        [NetworkAction.Failure]: ({ ...payload, loading: false, result: null, info: { changed: false } }),
    };

    return map[type];
}
