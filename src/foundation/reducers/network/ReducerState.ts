interface ReducerStateStateInfo {
    headers?: Headers,
    cacheKey?: string,
    changed?: boolean
}

export interface ReducerState<S> {
    loading: boolean;
    error: any;
    info: ReducerStateStateInfo,
    result: S | any;
}

export type ReducerStateMap<S> = {
    [key: string]: ReducerState<S>;
};