export enum ResponseHeader {
    TotalCount = "X-Total-Count",
}

export enum RequestHeader {
    Accept = "Accept",
    ContentType = "Content-Type",
}