// DISCLAIMER = usually provided by external libs + other models in this dir

export enum NetworkStatusCodes {
    OK = 200,
    Created = 201,
    Accepted = 202,
    NoContent = 204,
    BadRequest = 400,
}

export const NetworkSuccessStatusCodes = [
    NetworkStatusCodes.OK,
    NetworkStatusCodes.Accepted,
    NetworkStatusCodes.Created,
    NetworkStatusCodes.NoContent
];

