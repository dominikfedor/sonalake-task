export enum NetworkMethod {
    Update = "update",
    Create = "create",
    Show = "show",
    Index = "index",
    Delete = "delete"
};

export const MutableNetworkMethods = [ NetworkMethod.Create, NetworkMethod.Update, NetworkMethod.Delete ];

