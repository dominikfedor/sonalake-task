export enum QueryType {
    Page = "_page",
    Limit = "_limit",
    Order = "_order",
    Sort = "_sort",
    Search = "q",
}