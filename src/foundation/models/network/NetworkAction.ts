export enum NetworkAction {
    Loading = "network/LOADING",
    Success = "network/SUCCESS",
    Failure = "network/FAILURE"
}