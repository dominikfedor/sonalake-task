export interface Stats {
    totalCredits: number,
    dailyCredits: number,
}