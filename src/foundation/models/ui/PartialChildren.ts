import * as React from "react";

export interface PartialChildren {
    children?: React.ReactNode;
}
