export type KeyedLiteral<P = any> = {
    [key: string]: P;
};