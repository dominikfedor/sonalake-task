export type MapSet<Set> = {
    [key: string]: Set;
};