import React  from 'react';

import { BrowserRouter } from "react-router-dom";
import { Route } from 'react-router-dom';

import { UrlRoute } from "./UrlRoute";

import Container from "../layouts/container/Container";
import Header from "../layouts/header/Header";

import EditCharacterPage from "../page/character/edit/EditCharacterPage";
import CreateCharacterPage from "../page/character/create/CreateCharacterPage";
import CharactersListPage from "../page/character/list/CharacterListPage";
import ProfilePage from "../page/profile/ProfilePage";
import DashboardPage from "../page/dashboard/Dashboard";

export default function Router() {

    return (
        <BrowserRouter>
            <Header />
            <Container>
                <Route exact path={UrlRoute.Home} component={DashboardPage} />
                <Route exact path={UrlRoute.Profile} component={ProfilePage} />

                <Route exact path={UrlRoute.Characters} component={CharactersListPage} />
                <Route exact path={UrlRoute.CreateCharacter} component={CreateCharacterPage} />
                <Route exact path={UrlRoute.EditCharacter} component={EditCharacterPage} />
            </Container>
        </BrowserRouter>
    );
};