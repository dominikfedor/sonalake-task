export enum UrlRoute {
    Home = "/",
    Profile = "/profile",
    Characters = "/characters",
    CreateCharacter = "/create",
    EditCharacter = "/edit/:id",
}