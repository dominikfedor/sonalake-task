import React from "react";
import classNames from "classnames";

import { ClassName } from "../../../foundation/models/ui";

import TableHead from "./head/TableHead";
import TableBody from "./body/TableBody";

interface TableProps extends ClassName {
    head: React.ReactNode;
    body: React.ReactNode;
}

export const Table = ({ head, body, className }: TableProps) => (
    <table className={classNames("table", className)}>
        <TableHead>
            {head}
        </TableHead>
        <TableBody>
            {body}
        </TableBody>
    </table>
);

export default Table;
