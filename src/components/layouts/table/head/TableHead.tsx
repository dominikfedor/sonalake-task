import React from "react";

import { PartialChildren } from "../../../../foundation/models/ui";

const TableHead = ({ children }: PartialChildren) => (
    <thead className="thead-light">
        {children}
    </thead>
);

export default TableHead;
