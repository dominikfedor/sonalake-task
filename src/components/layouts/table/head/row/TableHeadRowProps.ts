import { Tuple } from "../../../../../foundation/models/data";

export type TableHeadCellProps = Tuple<string, string | null>;

export interface TableHeadRowProps {
    cells: TableHeadCellProps[];
}