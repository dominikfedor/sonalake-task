import React from "react";

import { useQueries } from "../../../../../hooks/network/UrlParams";
import { QueryType } from "../../../../../foundation/models/network";

import { TableHeadRowProps } from "./TableHeadRowProps";

export default function TableHeadRow({ cells }: TableHeadRowProps) {
    const [ queries, setQueries ] = useQueries();

    const currentOrder = queries.get(QueryType.Order);
    const currentKey = queries.get(QueryType.Sort);

    const handleSort = (nextKey: string) => setQueries({
        [QueryType.Sort]: nextKey,
        [QueryType.Order]: (currentKey === nextKey) && currentOrder === "asc" ? "desc" : "asc"
    });

    return (
        <tr>
            {cells.map(([ title, key ], i) => (
                <th onClick={() => key && handleSort(key)} scope="col" key={i}>
                    {title}
                    {currentKey === key && <i className={`fa fa-fw fa-sort-${currentOrder}`}/>}
                </th>
            ))}
        </tr>
    );
}
