import React from "react";

import { PartialChildren } from "../../../../foundation/models/ui";

const TableBody = ({ children }: PartialChildren) => (
    <tbody>
        {children}
    </tbody>
);

export default TableBody;
