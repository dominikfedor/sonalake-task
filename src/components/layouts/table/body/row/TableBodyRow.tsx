import React from "react";

import { TableBodyRowProps } from "./TableBodyRowProps";

const TableBodyRow = ({ id, cells }: TableBodyRowProps) => (
    <tr>
        <th scope="row">{id}</th>
        {cells.map((cell, i) => <td key={i}>{cell}</td>)}
    </tr>
);

export default TableBodyRow;
