import * as React from "react";

export interface TableBodyRowProps {
    cells: React.ReactNode[];
    id: string | number | undefined;
}