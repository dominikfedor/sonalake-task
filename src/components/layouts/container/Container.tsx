import styled from "styled-components";

export default styled.div`
  padding: 45px;
  display: flex;
  flex-direction: column;

  ${({ theme }) => `
      background-color: ${theme.background.primary};
      min-height: calc(100vh - ${theme.height.statusbar});
      z-index: ${theme.zIndex.container};
      color: ${theme.tint.contrast};
  `};
`;