import React from "react";
import styled from "styled-components";

import { ClassName } from "../../../foundation/models/ui";
import { UrlRoute } from "../../router/UrlRoute";

import UserControl from "components/items/user-control/UserControl";
import Logo from "components/items/logo/Logo";
import HorizontalFlow from "components/base/flow/horizontal";

import NavigationItem from "./navigation/item/NavigationItem";

const Header = ({ className }: ClassName) => (
    <nav className={className}>
        <Logo />
        <HorizontalFlow>
            <NavigationItem to={UrlRoute.Home}>
                Bulk verification
            </NavigationItem>
            <NavigationItem to={UrlRoute.Profile}>
                Profile
            </NavigationItem>
            <NavigationItem to={UrlRoute.Characters}>
                Pagination example
            </NavigationItem>
        </HorizontalFlow>
        <UserControl />
    </nav>
);

export default styled(Header)`
  font-size: 14px;
  position: relative;
  display: flex;
  flex-direction: row;

  ${({ theme }) => `
    background-color: ${theme.background.primary};
    height: ${theme.height.statusbar};
    z-index: ${theme.zIndex.statusbar};
    box-shadow: ${theme.shadow[1]};
  `};
`;