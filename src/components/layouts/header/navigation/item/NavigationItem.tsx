import React from "react";
import styled from "styled-components";
import { Link, withRouter } from "react-router-dom";

import { NavigationItemProps, LinkItemProps } from "./NavigationItemProps";

export default withRouter(({ className, children, to, location }: NavigationItemProps) => (
    <NavigationItem to={to} className={className} isActive={location.pathname === to} children={children} />));

const LinkItem = ({ children, className, to }: LinkItemProps) => (
    <Link className={className} children={children} to={to} />
);

const NavigationItem = styled(LinkItem)`
  font-size: 16px;
  text-align: center;
  display: flex;
  align-items: center;
  padding: 0 30px;
  height: 100%;

  ${({ theme, isActive }) => `
    border-bottom: ${isActive ? `3px solid ${theme.tint.primary}` : "none"};
    color: ${theme.tint.contrast};
    font-weight: ${isActive ? 600 : 400};
    text-decoration: none;

    &:hover {
        color: ${theme.tint.contrast};
        text-decoration: none;
    }
  `};
`;
