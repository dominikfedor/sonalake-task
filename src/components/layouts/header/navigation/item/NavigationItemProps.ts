import { RouteComponentProps } from "react-router";

import { ClassName, PartialChildren } from "../../../../../foundation/models/ui";
import { UrlRoute } from "../../../../router/UrlRoute";

export interface NavigationItemProps extends ClassName, PartialChildren, RouteComponentProps {
    to: UrlRoute | string;
}

export interface LinkItemProps extends ClassName, PartialChildren {
    isActive?: boolean;
    to: UrlRoute | string;
}