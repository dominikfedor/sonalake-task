import { createRef } from "react";

import { FormConfig } from "../../../../hooks/form/FormConfig";

export const CharacterFormConfig: FormConfig = {
    name: {
        label: "Name",
        id: "name-input",
        name: "name",
        placeholder: "Name...",
        type: "text",
        required: true,
        ref: createRef(),
    },
    gender: {
        options: [[ "Male", "male"], [ "Female", "female" ], [ "n/a", "n/a" ]],
        id: "gender-input",
        label: "Gender",
        name: "gender",
        required: true,
        ref: createRef(),
    },
    species: {
        label: "Species",
        id: "species-input",
        name: "species",
        required: true,
        ref: createRef(),
    },
    homeworld: {
        label: "Homeworld",
        id: "homeworld-input",
        name: "homeworld",
        placeholder: "Homeworld...",
        type: "text",
        required: false,
    },
};