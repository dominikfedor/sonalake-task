import React from "react";

import { useFormState } from "../../../../hooks/form/Form";
import { useSpeciesFetch } from "../../../../hooks/network";
import { NetworkMethod } from "../../../../foundation/models/network";
import { Character } from "../../../../foundation/models/characters/Character";

import Input from "../../../base/input/Input";
import Button from "../../../base/button/Button";
import Loader from "../../../items/loader/Loader";
import SelectButton from "../../../items/select-button/SelectButton";
import RadioInput from "../../../items/radio-input/RadioInput";

import { CharacterFormProps } from "./CharacterFormProps";
import { CharacterFormConfig } from "./CharacterFormConfig";

export default function CharacterForm({ initialState, onSave, isPending }: CharacterFormProps) {
    const [ { result, loading } ] = useSpeciesFetch({ method: NetworkMethod.Index });
    const [ state, handleSubmit ] = useFormState<Character>(initialState, CharacterFormConfig, onSave);

    if (loading || !result) {
        return <Loader />;
    }

    return (
        <div>
            <Input className="form-control" {...state.name} />
            <RadioInput {...state.gender} />
            <SelectButton options={result} {...state.species} />
            <Input className="form-control" {...state.homeworld} />
            <div className="row ml-0">
                <Button title="Save" disabled={isPending || loading} onClick={handleSubmit} className="btn-secondary mt-4" />
                <Loader className="mt-4 ml-3" isLoading={isPending} />
            </div>
        </div>
    );
}
