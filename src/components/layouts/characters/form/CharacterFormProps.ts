import { Character } from "../../../../foundation/models/characters/Character";
import { ClickHandler } from "../../../../foundation/models/ui";
import { FormState } from "../../../../hooks/form/FormState";

export interface CharacterFormProps {
    initialState: FormState<Character>;
    onSave: ClickHandler<Character | undefined>;
    isPending?: boolean;
}