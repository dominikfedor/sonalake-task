import React, { Dispatch, SetStateAction } from "react";

import { INITIAL_NETWORK_REDUCER_STATE } from "../../foundation/reducers/network/InitialState";
import { KeyedLiteral } from "../../foundation/models/data";
import { NetworkFetchHook } from "../../hooks/network/fetch/NetworkFetchHook";
import { ReducerState } from "../../foundation/reducers/network/ReducerState";

export interface ContextProviderProps {
    children: React.ReactElement;
}

export type StateContextType<S = KeyedLiteral> = [S, Dispatch<SetStateAction<S>>] | [ null, () => void ];
export type NetworkStateContextType<S = KeyedLiteral> = NetworkFetchHook<S> | [ ReducerState<S>, () => void ];

export const InitialStateContext: StateContextType = [ null, () => {} ];
export const InitialNetworkContext: NetworkStateContextType = [ INITIAL_NETWORK_REDUCER_STATE, () => {} ];
