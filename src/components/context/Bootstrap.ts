import { createContext, useState } from "react";
import { ThemeContext } from "styled-components";

import { NetworkMethod } from "../../foundation/models/network";
import { Preferences } from "../../foundation/models/preferences/Preferences";
import { Stats } from "../../foundation/models/stats/Stats";
import { useStatsFetch } from "../../hooks/network";
import { DefaultTheme } from "../../assets/styles/Theme";

import { composeContexts } from "./ComposeContexts";
import { Context } from "./Context";
import {
    NetworkStateContextType,
    ContextProviderProps,
    StateContextType,
    InitialNetworkContext,
    InitialStateContext
} from "./ContextProviderProps";

export const ContextMap = {
    [Context.StatsNetwork]: createContext<NetworkStateContextType<Stats>>(InitialNetworkContext),
    [Context.Preferences]: createContext<StateContextType<Preferences>>(InitialStateContext),
    [Context.Theme]: ThemeContext
};

export const ContextProvider = ({ children }: ContextProviderProps) => {

    const ContextValues = {
        [Context.StatsNetwork]: useStatsFetch({ method: NetworkMethod.Show }),
        [Context.Preferences]: useState<Preferences>({ showMessage: false }),
        [Context.Theme]: DefaultTheme,
    };

    return composeContexts(ContextMap, ContextValues, children)
}
