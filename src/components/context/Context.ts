export enum Context {
    Preferences  = "preferences",
    StatsNetwork = "stats-network",
    Theme = "app-theme",
}