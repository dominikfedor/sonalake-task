import React from "react";

import { KeyedLiteral } from "../../foundation/models/data";

export const composeContexts = (contexts: KeyedLiteral, values: KeyedLiteral, children: JSX.Element) => (
    Object.keys(contexts).reduce((acc, key) => {
        const Context = contexts[key];

        return <Context.Provider children={acc} value={values[key]} />;
    }, children)
);
