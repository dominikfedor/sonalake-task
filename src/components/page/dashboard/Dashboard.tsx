import React from "react";
import styled from "styled-components";

import { ClassName } from "../../../foundation/models/ui";

import EmailLists from "./email-lists/EmailLists";
import ListUpload from "./list-upload/ListUpload";

const DashboardPage = ({ className }: ClassName) => (
    <div className={className}>
        <ListUpload />
        <EmailLists />
    </div>
);

export default styled(DashboardPage)`
  display: flex;
  height: 100%;
  flex: 1;
  flex-direction: row;
`;