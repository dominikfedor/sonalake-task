import React from "react";
import styled from "styled-components";

import { ClassName } from "../../../../foundation/models/ui";
import { Tuple } from "../../../../foundation/models/data";

import TabMenu from "components/items/tab-menu/TabMenu";
import Banner from "components/items/banner/Banner";
import Text from "components/base/text/Text";
import HorizontalFlow from "components/base/flow/horizontal";

import EmailListItem from "./item/EmailListItem";

const filterOptions: Tuple<string, string>[] = [
    [ "All", "all" ], [ "Completed", "completed" ], ["Processing", "processing"], ["Unprocessed", "unprocessed"]];

const EmailLists = ({ className }: ClassName) => {

    return (
        <div className={className}>
            <ListsControl>
                <HorizontalFlow justifySelf="flex-start" className="ml-0 mr-auto">
                    <Text type="h1" title="Lists" />
                </HorizontalFlow>
                <Banner body="You have 5 unprocessed lists." actionTitle="Process all" />
                <TabMenu type="primary" initialValue="all" options={filterOptions} />
            </ListsControl>
            <EmailListItem />
            <EmailListItem />
        </div>
    );
};

const ListsControl = styled.div`
  margin-bottom: 30px;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`;

export default styled(EmailLists)`
  display: flex;
  flex-direction: column;
  flex: 7;
`;