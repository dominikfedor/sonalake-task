import React from "react";
import styled from "styled-components";

import { ClassName } from "../../../../../foundation/models/ui";

import HorizontalFlow from "components/base/flow/horizontal";
import VerticalFlow from "components/base/flow/vertical";
import Button from "components/base/button/Button";
import Text from "components/base/text/Text";

import icon from "images/csv-icon.svg";

const EmailListItem = ({ className }: ClassName) => (
    <HorizontalFlow className={className}>
        <VerticalFlow>
            <HorizontalFlow>
                <CsvIcon src={icon} alt="logo" />
                <Text type="h2" title="Chuan Shoes Dec Newsletter" />
            </HorizontalFlow>
            <VerticalFlow className="mt-3">
                <Text type="h5" title="4,121" />
                <Text type="body" title="Unique emails" />
            </VerticalFlow>
        </VerticalFlow>
        <HorizontalFlow alignSelf="flex-end">
            <Button type="primary">
                Start processing
            </Button>
        </HorizontalFlow>
    </HorizontalFlow>
);

const CsvIcon = styled.img`
  width: 24px;
  height: 24px;
  box-sizing: content-box;
  margin-right: 10px;
`;

export default styled(EmailListItem)`
  padding: 30px;
  margin-bottom: 20px;
  border-radius: 8px;

  ${({ theme }) => `
    background-color: ${theme.background.secondary};
  `};
`;