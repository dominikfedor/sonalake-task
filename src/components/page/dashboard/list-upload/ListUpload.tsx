import React from "react";

import styled from "styled-components";

import { ClassName } from "../../../../foundation/models/ui";
import { Tuple } from "../../../../foundation/models/data";

import TabMenu from "components/items/tab-menu/TabMenu";

import DropZone from "./drop-zone/DropZone";

const tabOptions: Tuple<string, string>[] = [
    ["File upload", "upload"],
    ["Cloud", "cloud"],
    ["Integrations", "integrations"]
];

const ListUpload = ({ className }: ClassName) => {

    return (
        <div className={className}>
            <TabMenu options={tabOptions} initialValue="cloud" type="secondary" />
            <DropZone />
        </div>
    );
};

export default styled(ListUpload)`
  margin-right: 45px;
  border-radius: 8px;
  text-align: center;
  display: flex;
  flex-direction: column;
  flex: 3;

  ${({ theme }) => `
    background-color: ${theme.background.secondary};
  `};
`;