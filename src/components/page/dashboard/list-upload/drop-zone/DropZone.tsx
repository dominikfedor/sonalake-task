import React from "react";
import styled from "styled-components";

import { ClassName } from "../../../../../foundation/models/ui";

import Text from "components/base/text/Text";
import VerticalFlow from "components/base/flow/vertical";
import Button from "components/base/button/Button";

import icon from "images/upload-icon.svg";

const DropZone = ({ className }: ClassName) => (
    <div className={className}>
        <VerticalFlow verticalAlign="center" spacing="center">
            <UploadIcon src={icon} alt="logo" className="mb-4" />
            <Text type="h1" title="Drag & Drop your file here" />
            <Text type="h5" title="- or -" className="mt-2" />
            <Button type="secondary" className="mt-4" title="Choose file" />
        </VerticalFlow>
    </div>
);

const UploadIcon = styled.img`
  width: 140px;
  height: auto;
  box-sizing: content-box;
`;

export default styled(DropZone)`
  margin: 16px;
  height: 100%;
  border-radius: 8px;

  & > div {
    height: 100%;
    margin: auto;
    max-width: 230px;
  }

  ${({ theme }) => `
      border: 1px dashed ${theme.tint.outline};
  `}
`;