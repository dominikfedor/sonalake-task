import React, { useContext } from "react";

import { Context, ContextMap } from "../../context";

import { useGQL } from "../../../hooks/network";
import { GqlLoadSet } from "../../../hooks/network/graphql/GqlLoadSet";

import Button from "../../base/button/Button";
import Loader from "../../items/loader/Loader";

export default function ProfilePage() {
    const [ preferences, setPreferences ] = useContext(ContextMap[Context.Preferences]);
    const [ { result: statsNetwork } ] = useContext(ContextMap[Context.StatsNetwork]);

    const name = "pikachu";

    const { result, loading } = useGQL(GqlLoadSet.Query, `{ pokemon(name: "${name}") { image, name } }`);

    const { showMessage } = preferences!;
    const toggleMsg = () => setPreferences({ showMessage: !showMessage });

    return (
        <div>
            <div className="row ml-0">
                <h1>Profile</h1>
            </div>

            <h4 className="mt-4">GQL result</h4>
            {loading ? <Loader className="mt-1 ml-3" /> : <img src={result?.pokemon.image} alt={name} />}

            <h4 className="mt-4">Context state</h4>
            <p>{JSON.stringify(preferences)}</p>

            <h4 className="mt-4">REST result</h4>
            {showMessage && statsNetwork && <p>{JSON.stringify(statsNetwork)}</p>}

            <Button onClick={toggleMsg} type="primary">
                {!showMessage ? "Show message" : "Hide message"}
            </Button>
        </div>
    );
};
