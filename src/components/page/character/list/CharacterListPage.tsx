import React, { useEffect } from "react";
import { generatePath, Link, useHistory } from "react-router-dom";

import { useCharactersFetch } from "../../../../hooks/network";
import { useQueries } from "../../../../hooks/network/UrlParams";
import { ResponseHeader, NetworkMethod, QueryType } from "../../../../foundation/models/network";
import { Character } from "../../../../foundation/models/characters/Character";
import { TableHeadCellProps } from "../../../layouts/table/head/row/TableHeadRowProps";
import { UrlRoute } from "../../../router/UrlRoute";

import Table from "../../../layouts/table/Table";
import Button from "../../../base/button/Button";
import Pagination from "../../../items/pagination/Pagination";
import TableHeadRow from "../../../layouts/table/head/row/TableHeadRow";
import TableBodyRow from "../../../layouts/table/body/row/TableBodyRow";
import EditActions from "../../../items/edit-actions/EditActions";
import SearchInput from "../../../items/search-input/SearchInput";
import Loader from "../../../items/loader/Loader";

const tableHeadCells: TableHeadCellProps[] = [
    [ "Id", "id" ], [ "Name", "name" ], ["Species", "species"], ["Gender", "gender"], ["Homeworld", "homeworld"], ["Actions", null]];

export default function CharactersListPage() {
    const [{ result, loading, info: { changed, headers } }, setFetchData ] = useCharactersFetch();
    const [ qs, setQueries ] = useQueries();
    const history = useHistory();
    const queries = qs.toString();

    useEffect(() => setQueries({ [QueryType.Limit]: "10" }), [ setQueries ]);
    useEffect(() => {
        if (queries || changed) {
            setFetchData({ method: NetworkMethod.Index, queries })
        }
    }, [ setFetchData, queries, changed ]);

    const renderTableBody = result && result.map(({ id, name, species, gender, homeworld }: Character) => (
        <TableBodyRow key={id} id={id} cells={[ name, species, gender, homeworld, EditActions({ id, onEdit, onDelete }) ]} />
    ));

    return (
        <div>
            <div className="row ml-0">
                <h1>List View</h1>
                <Loader className="mt-1 ml-3" isLoading={loading} />
            </div>
            <div className="row">
                <SearchInput placeholder="Search..." id="search-input" label="Search" labelClassName="sr-only" />
                <div className="col-sm-6 text-sm-right">
                    <Link to={UrlRoute.CreateCharacter}>
                        <Button type="secondary" title="Add new" className="mb-3" />
                    </Link>
                </div>
            </div>
            <Table
                head={<TableHeadRow cells={tableHeadCells} />}
                body={renderTableBody}
                className="table-bordered table-hover"
            />
            {(!loading || !queries) && !result && <h4>No Results Found.</h4>}
            {result && <Pagination totalCount={headers?.get(ResponseHeader.TotalCount)} />}
        </div>
    );

    function onEdit(id: string | number) { history.push(generatePath(UrlRoute.EditCharacter, { id })); }
    function onDelete(id: string | number) { setFetchData({ id, method: NetworkMethod.Delete }) }
};
