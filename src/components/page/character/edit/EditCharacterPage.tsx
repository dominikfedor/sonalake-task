import React, { useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";

import { NetworkMethod } from "../../../../foundation/models/network";
import { useCharactersFetch } from "../../../../hooks/network";
import { Character } from "../../../../foundation/models/characters/Character";

import CharacterForm from "../../../layouts/characters/form/CharacterForm";
import { UrlRoute } from "../../../router/UrlRoute";

export default function EditCharacterPage() {
    const { id } = useParams();
    const history = useHistory();

    const [{ result, loading, info: { changed } }, setFetchData ] = useCharactersFetch({ method: NetworkMethod.Show, id });

    useEffect(() => {
        if (changed) { history.push(UrlRoute.Characters); }
    }, [ changed, history ]);

    if (!result) {
        return null;
    }

    const handleSave = (body: Character | undefined) => setFetchData({ method: NetworkMethod.Update, body, id });

    return (
        <div>
            <h1>Edit character #{id}</h1>
            <CharacterForm initialState={{ values: result, errors: {} }} onSave={handleSave} isPending={loading} />
        </div>
    );
};
