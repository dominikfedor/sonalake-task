import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";

import { useCharactersFetch } from "../../../../hooks/network";
import { NetworkMethod} from "../../../../foundation/models/network";
import { Character } from "../../../../foundation/models/characters/Character";

import CharacterForm from "../../../layouts/characters/form/CharacterForm";
import { UrlRoute } from "../../../router/UrlRoute";

export default function CreateCharacterPage() {
    const [{ info: { changed }, loading }, setFetchData ] = useCharactersFetch();
    const history = useHistory();

    useEffect(() => {
        if (changed) { history.push(UrlRoute.Characters); }
    }, [ changed, history ]);

    const handleSave = (body: Character | undefined) => setFetchData({ method: NetworkMethod.Create, body });

    return (
        <div>
            <h1>Create character</h1>
            <CharacterForm onSave={handleSave} isPending={loading}  initialState={{ errors: {}, values: {} }} />
        </div>
    );
};
