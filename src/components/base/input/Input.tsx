import React, { forwardRef } from "react";
import classNames from "classnames";

import Label from "../label/Label";

import { InputProps } from "./InputProps";

const Input = forwardRef(({ id, required, labelClassName, className, label, value, error, ...props }: InputProps, ref: React.Ref<HTMLInputElement>) => (
    <div>
        {label && <Label value={label} className={labelClassName} htmlFor={id} id={`${id}-label`} required={required} />}
        <input
            className={classNames(className, { 'is-invalid': !!error })}
            ref={ref}
            id={id}
            value={value || ""}
            {...props}
        />
        {error && <div className="invalid-feedback">{error}</div>}
    </div>
));

export default Input;
