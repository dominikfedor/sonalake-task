import React, { FormEventHandler } from "react";

import { ClassName } from "../../../foundation/models/ui";

export interface InputProps extends ClassName {
    type?: "text" | "number" | "password" | "email" | "radio";
    id?: string;
    label?: string;
    onChange?: FormEventHandler;
    labelClassName?: string;
    placeholder?: string;
    ref?: React.Ref<HTMLInputElement>;
    error?: string;
    name?: string;
    required?: boolean;
    inputClassName?: string;
    value?: string | number;
}