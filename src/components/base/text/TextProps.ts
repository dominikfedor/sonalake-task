import { ClassName, PartialChildren } from "../../../foundation/models/ui";
import { MouseEventHandler } from "react";

export interface TextProps extends ClassName, PartialChildren {
    title?: string;
    disabled?: boolean;
    onClick?: MouseEventHandler;
    type: "h1" | "h2" | "h5" | "body" | "label";
    underline?: boolean;
    boldWeight?: boolean;
}