import React from "react";

import styled from "styled-components";

import { TextProps } from "./TextProps";

const Text = ({ children, title, className, boldWeight, ...props }: TextProps) => (
    <span className={className} {...props}>
        {title || children}
    </span>
);

export default styled(Text)`
  ${({ theme: { tint, font }, type }) => (({
    h1: `
        font-family: ${font.secondary};
        font-weight: 800;
        font-size: 30px;
    `,
    h2: `
        font-family: ${font.secondary};
        font-weight: 800;
        font-size: 24px;
    `,
    h5: `
        font-family: ${font.secondary};
        font-weight: 800;
        font-size: 18px;
    `,
    body: `
        font-family: ${font.primary};
        color: ${tint.secondary};
        font-weight: 400;
        font-size: 14px;
    `,
    label: `
        font-family: ${font.primary};
        font-weight: 600;
        font-size: 12px;
    `,
  })[type])}

    ${({ underline, boldWeight }) => `
        text-decoration: ${underline ? "underline" : "none"};
        ${boldWeight ? "font-weight: bold" : ""};
  `};
`