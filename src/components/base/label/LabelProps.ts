import { ClassName } from "../../../foundation/models/ui";

export interface LabelProps extends ClassName {
    id?: string;
    value?: string | number;
    htmlFor?: string;
    required?: boolean;
}