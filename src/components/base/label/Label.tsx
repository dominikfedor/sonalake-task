import React from "react";
import classNames from "classnames";

import { LabelProps } from "./LabelProps";

const Label = ({ htmlFor, className, required, value, id }: LabelProps) => (
    <label htmlFor={htmlFor} className={classNames("control-label", className, { required })} id={id}>
        {value}
    </label>
);

export default Label;
