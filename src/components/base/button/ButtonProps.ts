import { ClassName, PartialChildren } from "../../../foundation/models/ui";
import { MouseEventHandler } from "react";

export interface ButtonProps extends ClassName, PartialChildren {
    title?: string;
    disabled?: boolean;
    onClick?: MouseEventHandler;
    type: "primary" | "secondary" | "underlined" | "outlined" | "raised";
}