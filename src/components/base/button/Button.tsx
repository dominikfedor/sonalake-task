import React from "react";

import styled from "styled-components";

import { ButtonProps } from "./ButtonProps";

const Button = ({ children, title, type, ...props }: ButtonProps) => (
    <button type="button" { ...props }>
        {title || children}
    </button>
);

export default styled(Button)`
  letter-spacing: normal;
  border-radius: 5px;
  padding: 12px 15px;
  border: 2px solid transparent;
  font-size: 14px;
  font-weight: 700;
  font-family: ${({ theme }) => theme.font.primary};

  ${({ theme: { tint, background }, type }) => (({
    primary: `
        color: ${tint.contrast};
        background-color: ${tint.accent};
    `,
    secondary: `
        color: ${tint.contrast};
        background-color: ${tint.primary};
    `,
    outlined: `
        color: ${tint.secondary};
        background-color: ${background.secondary};
        border: 1px solid ${tint.primary};
    `,
    underlined: `
        color: ${tint.secondary};
        background-color: transparent;
        font-weight: 400;
        text-decoration: underline;
        padding: 12px 0;
    `,
    raised: `
        color: ${tint.contrast};
        background-color: ${background.secondary};
    `,
  })[type])}
`;