import React from "react";
import styled from "styled-components";

import { BadgeProps } from "./BadgeProps";

const Badge = ({ children, className }: BadgeProps) => (
    <div className={className}>
        {children}
    </div>
);

export default styled(Badge)`
  border-radius: 4px;
  align-self: center;
  font-weight: 700;
  font-family: ${({ theme }) => theme.font.primary};

  ${({ theme: { tint, background }, type }) => (({
    primary: `
        color: ${tint.secondary};
        background-color: ${background.secondary};
        font-size: 12px;
        height: 24px;
        padding: 0 12px;
    `,
    secondary: `
        color: ${tint.contrast};
        background-color: ${tint.primary};
        font-size: 14px;
        font-weight: 600;
        padding: 2px 10px;
    `,
})[type])}
`