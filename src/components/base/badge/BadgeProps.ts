import { ClassName, PartialChildren } from "../../../foundation/models/ui";

export interface BadgeProps extends ClassName, PartialChildren {
    type: "primary" | "secondary";
}