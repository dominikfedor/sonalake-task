import * as React from "react";
import styled from "styled-components";

import { VerticalFlowStateProps } from "./VerticalFlowStateProps";

const VerticalFlow = ({ id, className, children, onClick }: VerticalFlowStateProps) => (
    <div id={id} onClick={onClick} className={className}>
        {children}
    </div>
);

export default styled(VerticalFlow)`
  display: flex;
  flex-direction: column;

  ${({ verticalAlign = "flex-start", spacing = "flex-start", fullWidth }) => `
    align-items: ${spacing};
    justify-content: ${verticalAlign};
    width: ${fullWidth ? "100%" : "inherit"};
  `};
`