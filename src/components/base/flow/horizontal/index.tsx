import * as React from "react";
import styled from "styled-components";

import { HorizontalFlowProps } from "./HorizontalFlowProps";

const HorizontalFlow = ({ className, children, onClick, id }: HorizontalFlowProps) => (
    <div className={className} onClick={onClick} id={id}>
        {children}
    </div>
);

export default styled(HorizontalFlow)`
  display: flex;
  flex-direction: row;

  ${({ verticalAlign, spacing, wrap, justifySelf, alignSelf, fullWidth }) => `
    align-items: ${verticalAlign || "center"};
    justify-content: ${spacing || "space-between"};
    flex-wrap: ${wrap || "nowrap"};
    justify-self: ${justifySelf || "auto"};
    align-self: ${alignSelf || "auto"};
    width: ${fullWidth ? "100%" : "inherit"};
  `};
`