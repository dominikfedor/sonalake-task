import * as React from "react";

import { ClassName, PartialChildren } from "../../../../foundation/models/ui";

export interface HorizontalFlowProps extends ClassName, PartialChildren {
    onClick?: (e: React.MouseEvent<HTMLDivElement>) => void;
    fullWidth?: boolean;
    spacing?: "flex-start" | "flex-end" | "center" | "space-between" | "space-around" | "space-evenly";
    verticalAlign?: "flex-start" | "flex-end" | "center" | "baseline" | "stretch";
    wrap?: "nowrap" | "wrap" | "wrap-reverse";
    alignSelf?: "flex-start" | "flex-end" | "center" | "baseline" | "stretch";
    justifySelf?: "flex-start" | "flex-end" | "center" | "baseline" | "stretch";
    id?: string;
}
