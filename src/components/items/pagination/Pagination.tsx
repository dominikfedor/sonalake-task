import React from "react";
import classNames from "classnames";

import { useQueries } from "../../../hooks/network/UrlParams";
import { QueryType } from "../../../foundation/models/network";

import Button from "../../base/button/Button";

interface PaginationProps {
    totalCount: string | undefined | null;
}

export default function Pagination({ totalCount }: PaginationProps) {
    const [ queries, setQueries ] = useQueries();

    const page = parseInt(queries.get(QueryType.Page) || "1");
    const limit = parseInt(queries.get(QueryType.Limit) || "10");
    const totalPages = limit && totalCount ? Math.ceil(parseInt(totalCount) / limit) : 1;

    const setPage = (page: number) => () => setQueries({ [QueryType.Page]: page.toString() });

    const renderPages = () => [null, ...Array(totalPages)].map((_, i) => !!i && (
        <li className={classNames("page-item", { active: i === page })} key={i}>
            <Button className="page-link" onClick={setPage(i)}>
                {i} <span className="sr-only">(current)</span>
            </Button>
        </li>
    ));

    return (
        <nav aria-label="Data grid navigation">
            <ul className="pagination justify-content-end">
                <li className={classNames("page-item", { disabled: page < 2 })}>
                    <Button className="page-link" onClick={setPage(page - 1)}>
                        Previous
                    </Button>
                </li>
                {renderPages()}
                <li className={classNames("page-item", { disabled: page === totalPages })}>
                    <Button className="page-link" onClick={setPage(page + 1)}>
                        Next
                    </Button>
                </li>
            </ul>
        </nav>
    );
};
