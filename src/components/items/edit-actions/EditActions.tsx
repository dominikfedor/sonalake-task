import React from "react";

import Button from "../../base/button/Button";

import { EditActionsProps } from "./EditActionsProps";

const EditActions = ({ id, onEdit, onDelete }: EditActionsProps) => (
    <div
        className="btn-group btn-group-sm"
        role="group"
        aria-label="Actions"
    >
        <Button type="raised" onClick={() => onEdit(id)}>
            <i className="fa fa-pencil" aria-hidden="true" /> Edit
        </Button>
        <Button type="primary" onClick={() => onDelete(id)}>
            <i className="fa fa-trash-o" aria-hidden="true" /> Remove
        </Button>
    </div>
);

export default EditActions;
