import { ClickHandler } from "../../../foundation/models/ui";

export interface EditActionsProps {
    id: string | number,
    onEdit: ClickHandler<string | number>,
    onDelete: ClickHandler<string | number>,
}