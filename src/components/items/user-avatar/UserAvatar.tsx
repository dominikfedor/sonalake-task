import React from "react";

import styled from "styled-components";

import { ClassName } from "../../../foundation/models/ui";

const UserAvatar = ({ className, value }: ClassName & { value: string }) => (
    <div className={className}>
        {value}
    </div>
);

export default styled(UserAvatar)`
  height: 42px;
  width: 42px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 100%;
  font-size: 16px;
  font-weight: 700;

  ${({ theme: { tint, font } }) => `
    color: ${tint.contrast};
    background-color: ${tint.primary};
    font-family: ${font.secondary};
  `}
`;
