import React, { forwardRef } from "react";
import classNames from "classnames";
import Label from "../../base/label/Label";

import { SelectButtonProps } from "./SelectButtonProps";

const SelectButton = forwardRef(({  id, value, className, options, required, label, error, ...props }: SelectButtonProps, ref: React.Ref<HTMLSelectElement>) => (
    <div className="form-group">
        <Label value={label} htmlFor={id} id={`${id}-label`} required={required} />
        <select
            className={classNames(className, "form-control", { 'is-invalid': !!error, required })}
            // selected={value || ""}
            value={value || ""}
            ref={ref}
            id={id}
            {...props}
        >
            <option disabled value=""> -- select an option -- </option>
            {options.map(($, i) => <option key={i} value={$}>{$}</option>)}
        </select>
        {error && <div className="invalid-feedback">{error}</div>}
    </div>
));

export default SelectButton;
