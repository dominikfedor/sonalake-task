import { Tuple } from "../../../foundation/models/data";
import { ClassName, ClickHandler } from "../../../foundation/models/ui";

export interface TabMenuProps extends ClassName {
    initialValue: string;
    options: Tuple<string, string>[]
    onSelect?: ClickHandler<string>;
    type: "primary" | "secondary";
}
