import React, { useState } from "react";
import styled from "styled-components";

import TabControlItem from "./item/TabControlItem";

import { TabMenuProps } from "./TabMenuProps";

const TabMenu = ({ initialValue, options, type, onSelect, className }: TabMenuProps) => {
    const [ option, setOption ] = useState(initialValue);

    const handleClick = (value: string) => () => {
        if (onSelect) { onSelect(value) }
        setOption(value);
    };

    const renderTabs = () => options.map(([ title, value ]) => (
        <TabControlItem key={value} type={type} title={title} active={option === value} onClick={handleClick(value)} />
    ));

    return <div className={className}>{renderTabs()}</div>;
};

export default styled(TabMenu)`
    display: flex;

    ${({ theme: { background }, type }) => (({
        primary: `
            align-items: center;
            align-self: flex-end;
    `,
        secondary: `
            flex: 1;
            border-radius: 8px;
            background-color: ${background.tertiary};
    `,
    })[type])}
`;