import { MouseEventHandler } from "react";

import { ClassName } from "../../../../foundation/models/ui";

export interface TabMenuItemProps extends ClassName {
    title: string;
    type: "primary" | "secondary";
    onClick?: MouseEventHandler;
    active?: boolean;
}
