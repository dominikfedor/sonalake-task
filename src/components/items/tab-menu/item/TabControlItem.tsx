import React from "react";
import styled from "styled-components";

import Text from "../../../base/text/Text";

import { TabMenuItemProps } from "./TabMenuItemProps";

const TabControlItem = ({ className, title, active, onClick }: TabMenuItemProps) => (
    <Text type="body" title={title} boldWeight={active} className={className} onClick={onClick} />
);

export default styled(TabControlItem)`
  cursor: pointer;

  ${({ theme: { tint, background }, active, type }) => (({
    primary: `
        padding: 12px 16px;
        border-bottom: ${active ? `3px solid ${tint.primary}` : `1px solid ${background.secondary}`};
        color: ${active ? tint.contrast : tint.secondary};
    `,
    secondary: `
        background-color: ${active ? background.secondary : background.tertiary};
        color: ${active ? tint.contrast : tint.secondary};
        border-right: 1px solid ${background.secondary};
        padding: 20px 0;
        justify-content: center;
        align-items: center;
        text-align: center;
        cursor: pointer;
        flex: 1;

        &:first-child {
            border-radius: 8px 0 0;
        }

        &:last-child {
            border-right: none;
            border-radius: 0 8px 0;
        }
    `,
  })[type])}
`;