import React from "react";
import styled from "styled-components";

import { ClassName } from "../../../foundation/models/ui";
import { UrlRoute } from "../../router/UrlRoute";

import { Link } from "react-router-dom";

import logo from "images/logo.svg";

const LogoLink = ({ className }: ClassName) => (
    <Link className={className} to={UrlRoute.Home}>
        <Logo src={logo} alt="logo" />
    </Link>
);

const Logo = styled.img`
  padding: 0 45px 16px;
  width: 206px;
  box-sizing: content-box;
  height: auto;
`;

export default styled(LogoLink)`
  display: flex;
  text-align: center;
  align-items: center;
`;