import React, { forwardRef } from "react";
import classNames from "classnames";

import Input from "../../base/input/Input";
import Label from "../../base/label/Label";

import { RadioInputProps } from "./RadioInputProps";

const RadioInput = forwardRef(({ name, label, options, className, required, onChange, value, error, ...props }: RadioInputProps, ref: React.Ref<HTMLInputElement>) => (
    <div className="form-group mt-3">
        <span className={classNames("control-label", { required })}>
            {label}
        </span>
        <div className={classNames("form-control mt-2", { 'is-invalid': !!error })}>
            {options.map(([ optionLabel, optionValue ], i) => (
                <div className="d-inline-flex mr-4" key={i}>
                    <Label value={optionLabel} htmlFor={optionValue} id={`${optionValue}-label`} />
                    <Input
                        type="radio"
                        className="ml-2"
                        id={optionValue}
                        name={name}
                        value={optionValue}
                        onChange={onChange}
                        ref={ref}
                        // checked={value === optionValue}
                    />
                </div>
            ))}
            </div>
        {error && <div className="invalid-feedback">{error}</div>}
    </div>
));

export default RadioInput;
