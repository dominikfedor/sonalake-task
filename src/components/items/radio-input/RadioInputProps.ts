import { FormEventHandler } from "react";

import { ClassName } from "../../../foundation/models/ui";
import { Tuple } from "../../../foundation/models/data";

export interface RadioInputProps extends ClassName {
    id?: string;
    label?: string;
    onChange?: FormEventHandler;
    labelClassName?: string;
    placeholder?: string;
    options: Tuple<string, string>[];
    error?: string;
    name?: string;
    required?: boolean;
    inputClassName?: string;
    value?: string | number;
}