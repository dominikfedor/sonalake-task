import React from "react";
import styled from "styled-components";

import Button from "components/base/button/Button";
import Text from "components/base/text/Text";

import { BannerProps } from "./BannerProps";

const Banner = ({ className, body, onAction, actionTitle }: BannerProps) => (
    <div className={className}>
        <Text type="body" title={body} className="mr-4" />
        <Button type="raised" className="mr-2" onClick={onAction} title={actionTitle} />
    </div>
)

export default styled(Banner)`
  display: flex;
  padding: 0px 30px;
  border-radius: 8px;
  align-items: center;
`;