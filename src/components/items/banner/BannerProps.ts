import { ClassName, ClickHandler } from "../../../foundation/models/ui";

export interface BannerProps extends ClassName {
    body: string,
    actionTitle: string;
    onAction?: ClickHandler,
}