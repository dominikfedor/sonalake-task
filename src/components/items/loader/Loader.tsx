import React from "react";
import classNames from "classnames";

import { ClassName } from "../../../foundation/models/ui";

interface LoaderProps extends ClassName {
    isLoading?: boolean,
}

const Loader = ({ className, isLoading = true }: LoaderProps) => isLoading ? (
    <div className={classNames("spinner-border", className)} role="status">
        <span className="sr-only">Loading...</span>
    </div>
) : null;

export default Loader;