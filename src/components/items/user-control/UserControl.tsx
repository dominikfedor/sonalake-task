import React from "react";

import Badge from "components/base/badge/Badge";
import Button from "components/base/button/Button";
import HorizontalFlow from "components/base/flow/horizontal";

import UserAvatar from "../user-avatar/UserAvatar";

const UserControl = () => (
    <HorizontalFlow className="ml-auto mr-5">
        <Badge type="secondary" className="mr-3">
            54, 123 cr.
        </Badge>
        <Button type="outlined" className="mr-2">
            Add credits
        </Button>
        <UserAvatar value="M" className="ml-4" />
    </HorizontalFlow>
);

export default UserControl;