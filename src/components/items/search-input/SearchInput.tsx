import React, { useEffect } from "react";

import { useDebounceValue } from "../../../hooks/network/Debounce";
import { useQueries } from "../../../hooks/network/UrlParams";
import { QueryType } from "../../../foundation/models/network";

import { InputProps } from "../../base/input/InputProps";

import Input from "../../base/input/Input";

export default function SearchInput({ ...props }: InputProps) {
    const [ queries, setQueries ] = useQueries();
    const [ value, debouncedValue, setValue ] = useDebounceValue(queries.get(QueryType.Search) || "", 200);

    useEffect(() => {
        setQueries({ [QueryType.Search]: debouncedValue });
    }, [ debouncedValue, setQueries ]);

    return (
        <div className="col-sm-6">
            <div className="form-group">
                <Input
                    className="form-control"
                    // @ts-ignore
                    onChange={(e) => setValue(e.target.value)}
                    value={value}
                    {...props}
                />
            </div>
        </div>
    );
}
