export const MediaSize = {
    mobileS: '320px',
    mobileM: '375px',
    mobileL: '425px',
    tablet: '768px',
    laptop: '1024px',
    laptopL: '1440px',
    desktop: '2560px'
}

export const MediaScreen = {
    mobileS: `(min-width: ${MediaSize.mobileS})`,
    mobileM: `(min-width: ${MediaSize.mobileM})`,
    mobileL: `(min-width: ${MediaSize.mobileL})`,
    tablet: `(min-width: ${MediaSize.tablet})`,
    laptop: `(min-width: ${MediaSize.laptop})`,
    laptopL: `(min-width: ${MediaSize.laptopL})`,
    desktop: `(min-width: ${MediaSize.desktop})`,
    desktopL: `(min-width: ${MediaSize.desktop})`
};