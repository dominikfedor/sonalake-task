import { KeyedLiteral } from "../../foundation/models/data";

export interface ThemeInterface {
    tint: KeyedLiteral;
    background: KeyedLiteral;
    zIndex: KeyedLiteral;
    height: KeyedLiteral
    shadow: KeyedLiteral;
    font: KeyedLiteral;
}

export const DefaultTheme: ThemeInterface = {
    tint: {
        accent: "#F67A36",
        contrast: "#ffffff",
        outline: "#ffffff78",
        primary: "#16BAC4",
        secondary: "#C6EBED",
    },
    background: {
        primary: "#1a31cd",
        secondary: "#1429b0",
        tertiary: "#182694"
    },
    height: {
        statusbar: "98px",
    },
    zIndex: {
        statusbar: 5,
        container: 1,
        modal: 10
    },
    shadow: {
        1: "0px 20px 40px 5px rgba(0,0,0,0.08)"
    },
    font: {
        primary: "'Open Sans', sans-serif",
        secondary: "'Montserrat', sans-serif"
    }
};