import React from "react";
import ReactDOM from "react-dom";

import { GraphQL, GraphQLProvider } from 'graphql-react';

import * as serviceWorker from "./serviceWorker";

import Router from "./components/router";
import { ContextProvider } from "./components/context";

import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.css";

import "./index.css";

const graphql = new GraphQL();

ReactDOM.render(
    <GraphQLProvider graphql={graphql}>
        <ContextProvider>
            <Router />
        </ContextProvider>
    </GraphQLProvider>,
    document.getElementById('app')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
