import { useLocation, useHistory } from "react-router-dom";
import { useMemo } from "react";
import { History } from "history";

import { KeyedLiteral } from "../../foundation/models/data";

export type QueryTypeMap = KeyedLiteral<string>;

type QuerySetter = (newQs: QueryTypeMap) => void;

export const useQueries = (): [ URLSearchParams, QuerySetter, History ] => {
    const history = useHistory();
    const { push, location: { pathname } } = history;
    const { search } = useLocation();
    const queries = new URLSearchParams(search);

    const setQueries = useMemo<QuerySetter>(() => (newQs: QueryTypeMap) => {
        const qs = new URLSearchParams(search);

        Object.keys(newQs).forEach((key) => {
            const value = newQs[key];

            if (value) { qs.set(key, value); }
            else { qs.delete(key); }
        });

        push(`${pathname}?${qs.toString()}`);
    }, [ push, search, pathname ]);

    return [ queries, setQueries, history ];
}
