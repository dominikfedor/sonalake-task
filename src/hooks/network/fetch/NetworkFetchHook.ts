import { Dispatch, Reducer, SetStateAction } from "react";

import { ReducerState } from "../../../foundation/reducers/network/ReducerState";
import { Action } from "../../../foundation/reducers/network/Action";

import { FetchData } from "./FetchData";

export type NetworkFetchHook<State> = [ ReducerState<State>, Dispatch<SetStateAction<FetchData>> ];
export type NetworkFetchReducer<State> = Reducer<ReducerState<State>, Action>;