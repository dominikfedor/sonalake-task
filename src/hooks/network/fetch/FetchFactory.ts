import { RestMethod, NetworkMethod, RequestHeader } from "../../../foundation/models/network";
import { FetchData } from "./FetchData";

import { RouteEntity } from "../index";

const endpoint = process.env.REST_URL;
const headers = {
    [RequestHeader.Accept]: process.env.REST_VERSION,
    [RequestHeader.ContentType]: process.env.REST_VERSION,
};

const appendToUrl = (val: string | number | undefined, prefix: string) => val ? `${prefix}${val}` : "";

export const fetchFactory = ({ body, method, id, queries }: FetchData, entity: RouteEntity) => {
    const url = `${endpoint}${entity}${appendToUrl(id, "/")}${appendToUrl(queries, "?")}`;

    const map = {
        [NetworkMethod.Show]: ([ url, { method: RestMethod.GET } ]),
        [NetworkMethod.Index]: ([ url, { method: RestMethod.GET } ]),
        [NetworkMethod.Delete]: ([ url, { method: RestMethod.DELETE } ]),
        [NetworkMethod.Update]: ([ url, {
            method: RestMethod.PUT,
            body: JSON.stringify(body),
            headers }
        ]),
        [NetworkMethod.Create]: ([ url, {
            method: RestMethod.POST,
            body: JSON.stringify(body),
            headers }
        ]),
    };

    return map[method!] as [RequestInfo, RequestInit];
};
