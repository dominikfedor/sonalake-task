import { useEffect, useReducer, useState } from "react";

import { NetworkSuccessStatusCodes, NetworkAction, MutableNetworkMethods } from "../../../foundation/models/network";
import { INITIAL_NETWORK_REDUCER_STATE } from "../../../foundation/reducers/network/InitialState";
import { RouteEntity } from "../index";

import { fetchFactory } from "./FetchFactory";
import { FetchData } from "./FetchData";

import { NetworkFetchHook, NetworkFetchReducer } from "./NetworkFetchHook";

export const useFetchNetwork = <S>(reducer: NetworkFetchReducer<S>, entity: RouteEntity, initialData: FetchData = {}): NetworkFetchHook<S> => {
    const [ fetchData, setFetchData ] = useState<FetchData>(initialData);
    const [ state, dispatch ] = useReducer<NetworkFetchReducer<S>>(reducer, INITIAL_NETWORK_REDUCER_STATE);

    useEffect(() => {
        if (fetchData.method) {
            dispatch({ type: NetworkAction.Loading });

            (async () => {
                try {
                    const res = await fetch(...fetchFactory(fetchData, entity));

                    if (!NetworkSuccessStatusCodes.includes(res.status)) {
                        throw new Error(`Error code: ${res.status}`);
                    }

                    const data = await res.json();

                    dispatch({
                        type: NetworkAction.Success,
                        payload: {
                            result: Object.keys(data).length ? data : null,
                            info: {
                                headers: res.headers,
                                changed: MutableNetworkMethods.includes(fetchData.method!)
                            }
                        }
                    });
                } catch (error) {
                    dispatch({ type: NetworkAction.Failure, payload: { error } })
                }
            })()
        }
    }, [ fetchData, entity ]);

    return [ state, setFetchData ];
};