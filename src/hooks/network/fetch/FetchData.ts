import { NetworkMethod } from "../../../foundation/models/network";

export interface FetchData {
    method?: NetworkMethod;
    queries?: string;
    id?: string | number;
    body?: Object;
}