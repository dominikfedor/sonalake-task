import { MapSet } from "../../../foundation/models/data/MapSet";

import { GqlLoadSet } from "./GqlLoadSet";
import { QueryLoadOptions } from "./QueryOptions";

export const queryLoadSettings: MapSet<QueryLoadOptions> = {
    [GqlLoadSet.Query]: ({ loadOnMount: true, loadOnReload: true, loadOnReset: true, reloadOnLoad: false, resetOnLoad: false }),
    [GqlLoadSet.Mutate]: ({ loadOnMount: false, loadOnReload: false, loadOnReset: false, reloadOnLoad: true, resetOnLoad: false }),
    [GqlLoadSet.Reset]: ({ loadOnMount: false, loadOnReload: false, loadOnReset: false, reloadOnLoad: false, resetOnLoad: true }),
    [GqlLoadSet.None]: ({ loadOnMount: false, loadOnReload: false, loadOnReset: false, reloadOnLoad: false, resetOnLoad: false }),
};