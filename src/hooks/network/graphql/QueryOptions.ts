export interface QueryLoadOptions {
    loadOnMount?: boolean;
    loadOnReload?: boolean;
    loadOnReset?: boolean;
    reloadOnLoad?: boolean;
    resetOnLoad?: boolean;
}