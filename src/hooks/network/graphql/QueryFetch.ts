import { useEffect, useReducer } from "react";
import { GraphQLFetchOptions, useGraphQL } from "graphql-react";

import { NetworkAction } from "../../../foundation/models/network";
import { ReducerState } from "../../../foundation/reducers/network/ReducerState";
import { INITIAL_NETWORK_REDUCER_STATE } from "../../../foundation/reducers/network/InitialState";
import { KeyedLiteral } from "../../../foundation/models/data";

import { NetworkFetchReducer } from "../fetch/NetworkFetchHook";

import { queryLoadSettings } from "./QueryLoadSettings";
import { GqlLoadSet } from "./GqlLoadSet";

const setUrl = (options: GraphQLFetchOptions) => { options.url = process.env.GRAPHQL_URL || "" };

export const useGQLNetwork = <T, V>(reducer: NetworkFetchReducer<T>, loadSet: GqlLoadSet, query: string, variables?: KeyedLiteral): ReducerState<T> => {
    const [ state, dispatch ] = useReducer<NetworkFetchReducer<T>>(reducer, INITIAL_NETWORK_REDUCER_STATE);

    const { cacheValue, cacheKey } = useGraphQL({
        fetchOptionsOverride: setUrl,
        operation: { query, variables },
        ...queryLoadSettings[loadSet]
    });

    useEffect(() => {

        if (cacheValue) {
            const { data, ...errors } = cacheValue;

            if (data) {
                dispatch({
                    type: NetworkAction.Success,
                    payload: { result: data, error: null, loading: false, info: { cacheKey } }
                });
            }

            if (Object.keys(errors).length) {
                // TODO: WIP
                dispatch({ type: NetworkAction.Failure, payload: { error: JSON.stringify(errors) } })
            }
        } else {
            dispatch({ type: NetworkAction.Loading });
        }

    }, [ cacheValue, cacheKey ]);

    return state;
};