export enum GqlLoadSet {
    Query,
    Mutate,
    Reset,
    None
}