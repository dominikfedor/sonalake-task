import { fetchReducer } from "../../foundation/reducers/network";
import { Character } from "../../foundation/models/characters/Character";
import { Species } from "../../foundation/models/species/Species";
import { Stats } from "../../foundation/models/stats/Stats";
import { KeyedLiteral } from "../../foundation/models/data";

import { useGQLNetwork } from "./graphql/QueryFetch";
import { GqlLoadSet } from "./graphql/GqlLoadSet";

import { FetchData } from "./fetch/FetchData";
import { useFetchNetwork } from "./fetch/NetworkFetch";

export enum RouteEntity {
    Characters = "/characters",
    Species = "/species",
    Stats = "/characters/10", // "Stats" route mock
}

const serviceEntity = {
    [RouteEntity.Characters]: (options?: FetchData) => useFetchNetwork<Character>(fetchReducer, RouteEntity.Characters, options),
    [RouteEntity.Species]: (options?: FetchData) => useFetchNetwork<Species>(fetchReducer, RouteEntity.Species, options),
    [RouteEntity.Stats]: (options?: FetchData) => useFetchNetwork<Stats>(fetchReducer, RouteEntity.Stats, options),
};

export const useCharactersFetch = serviceEntity[RouteEntity.Characters];
export const useSpeciesFetch = serviceEntity[RouteEntity.Species];
export const useStatsFetch = serviceEntity[RouteEntity.Stats];

export const useGQL = <T, V>(options: GqlLoadSet, query: string, variables?: KeyedLiteral) => useGQLNetwork<T, V>(fetchReducer, options, query, variables);

