import { useState, useEffect } from 'react';

type DebounceValueHook = [ string, string, any ];

export const useDebounceValue = (initialValue: string, delay: number): DebounceValueHook => {
    const [ value, setValue ] = useState(initialValue);
    const [ debouncedValue, setDebouncedValue ] = useState(initialValue);

    useEffect(() => {
        const handler = setTimeout(() => setDebouncedValue(value), delay);

        return () => { clearTimeout(handler); };
    }, [value, delay]);

    return [ value, debouncedValue, setValue];
}
