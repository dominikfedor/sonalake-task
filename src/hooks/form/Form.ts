import { useState, useCallback, MouseEventHandler } from 'react';

import { KeyedLiteral } from "../../foundation/models/data";
import { ClickHandler } from "../../foundation/models/ui";
import { Character } from "../../foundation/models/characters/Character";

import { FormState } from "./FormState";
import { FormConfig } from "./FormConfig";

type FormStateHook<S> = [ KeyedLiteral, MouseEventHandler ];

export function useFormState<S>(initialState: FormState<S>, formConfig: FormConfig = {}, callback: ClickHandler<Character>): FormStateHook<S> {
    const [state, setState] = useState<FormState<S>>(initialState || { values: {}, errors: {} });

    const handleErrorCheck = useCallback(({ name, value }) => {
        if (formConfig[name].required && !value) {
            formConfig[name].ref?.current.focus();

            return 'This is required field.';
        }

        return null;
    }, [ formConfig ]);

    const validateState = useCallback(() => {
        return Object.keys(formConfig).some(key => {
            const value = state.values![key];
            const isInvalid = (formConfig[key].required && !value) || state.errors![key];

            if (isInvalid) {
                setState((prevState) => ({
                    ...prevState,
                    errors: {
                        ...prevState.errors,
                        [key]: handleErrorCheck({ name: key, value }),
                    }
                }));
            }

            return isInvalid;
        });
    }, [ state, formConfig, handleErrorCheck ]);

    const handleChange = useCallback(({ target: { name, value } }) => {
        setState(prevState => ({
            ...prevState,
            values: {
                ...prevState.values,
                [name]: value
            },
            errors: {
                ...prevState.errors,
                [name]: handleErrorCheck({ name, value }),
            }
        }));
    }, [ handleErrorCheck ]);

    const handleSubmit = useCallback((event) => {
        event.preventDefault();

        if (!validateState() && state.values) {
            callback(state.values);
        }
    }, [ state, callback, validateState ]);

    const formValues = Object.keys(formConfig).reduce((acc, key) => ({
        ...acc,
        [key]: {
            ...formConfig[key],
            value: state.values![key],
            error: state.errors![key],
            onChange: handleChange
        }
    }), {});

    return [ formValues, handleSubmit ];
}
