import { KeyedLiteral } from "../../foundation/models/data";

export interface FormState<S> {
    values?: any;
    errors?: KeyedLiteral<string | null>;
}