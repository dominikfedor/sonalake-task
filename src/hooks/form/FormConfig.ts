import { RefObject } from "react";

import { KeyedLiteral, Tuple } from "../../foundation/models/data";
import { InputProps } from "../../components/base/input/InputProps";

export type FormConfig = KeyedLiteral<InputProps & { ref?: RefObject<any>, options?: Tuple<string, string>[] }>
