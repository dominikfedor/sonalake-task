// Basic requires
const path = require('path');
const args = require('minimist')(process.argv.slice(2));

// List of allowed environments
const ENV_DEVELOPMENT = 'development';
const ENV_PRODUCTION = 'production';
const ENV_STAGING = 'staging';
const allowedEnvs = [ENV_DEVELOPMENT, ENV_STAGING, ENV_PRODUCTION];

// Set the correct environment
const env = args.env ? args.env : ENV_DEVELOPMENT;

// Set NODE env
process.env.REACT_WEBPACK_ENV = env;
process.env.NODE_ENV = env;
process.env.BROWSERSLIST_CONFIG = path.join(__dirname, './browserslist');

/**
 * Get the configuration object for used environment.
 *
 * @param {String} usedEnv
 * @return {Object} Webpack config
 */

/* eslint-disable import/no-dynamic-require */
const getConfig = (usedEnv) => {
    if (usedEnv === ENV_DEVELOPMENT) {
        return require(path.join(__dirname, 'webpack/development'));
    } else if (usedEnv === ENV_STAGING) {
        return require(path.join(__dirname, 'webpack/staging'));
    } else if (usedEnv === ENV_PRODUCTION) {
        return require(path.join(__dirname, 'webpack/production'));
    } else {
        return null;
    }
};
/* eslint-enable import/no-dynamic-require */

/**
 * Get an allowed environment
 * @param  {String} environment
 * @return {String}
 */
const getValidEnv = (environment) => {
    const isValid = environment && environment.length > 0 && allowedEnvs.indexOf(environment) !== -1;
    return isValid ? environment : ENV_DEVELOPMENT;
};

/**
 * Build the webpack configuration
 * @param  {String} environment Environment to use
 * @return {Object} Webpack config
 */
const buildConfig = (environment) => {
    const usedEnv = getValidEnv(environment);
    return getConfig(usedEnv);
};

module.exports = buildConfig(env);
