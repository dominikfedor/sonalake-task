## FE Stack PoC

Client & Server start scripts:
```bash
yarn  # install npm deps

yarn start # run the app
yarn run api # start the json-server
```